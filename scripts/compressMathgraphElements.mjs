/**
 * Un fichier pour lancer plus facilement la commande manuellement avec `node  ./scripts/compressMathgraphElements.mjs`
 * @fileOverview
 */
import compressMathgraphElements from "./compressMathgraphElements.js"
compressMathgraphElements()
