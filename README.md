Mathgraph
=========

Logiciel libre de géométrie, d'analyse et de simulation multiplateforme, par Yves Biton.

Plus d'infos sur https://www.mathgraph32.org/, avec des exemples et la possibilité de l'utiliser en ligne.

Pour l'utiliser sur votre site, cf https://www.mathgraph32.org/documentation/loading/index.html

Compilation
-----------

En cas d'erreur ERR_OSSL_EVP_UNSUPPORTED au build (avec un node récent), lancer dans votre console 
un `export NODE_OPTIONS=--openssl-legacy-provider` puis relancer la commande de build.
