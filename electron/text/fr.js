/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

const textFr = {
  ErrSave: "Une erreur est survenue pendant l'enregistrement du fichier : ",
  ErrOpen: "Une erreur est survenue pendant l'ouverture du fichier : ",
  ExpPNG: 'Exportez votre figure dans un fichier image',
  ExpHTML: 'Exportez votre figure dans une page HTML',
  MtgFiles: 'Fichiers MathGraph32',
  PNGFiles: 'Fichiers PNG',
  JPGFiles: 'Fichiers JPEG',
  SVGFiles: 'Fichiers SVG',
  HTMLFiles: 'Fichiers HTML',
  AllFiles: 'Tous les fichiers',
  AvertDirty: "La figure n'a pas été sauvegardée. Quitter sans sauvegarder ?",
  OpenFig: 'Ouvrez une figure mgj',
  SaveFig: 'Enregistrez votre figure',
  Choice: 'Choisissez entre :',
  Save: 'Enregistrer',
  SaveAs: 'Enregistrer sous',
  Discard: 'Annuler',
  Yes: 'Oui',
  No: 'Non',
  AddConst: 'Incorporer une figure depuis un fichier',
  SaveProto: 'Enregistrement de la construction ',
  MGCFiles: 'Fichiers mgc',
  undo: 'Annuler',
  redo: 'Refaire',
  cut: 'Couper',
  copy: 'Copier',
  paste: 'Coller',
  delete: 'Supprimer',
  selectall: 'Tout sélectionner',
  quit: "Quitter l'application",
  file: 'Fichier',
  expImSVG: 'Enregistrer dans un fichier image SVG',
  expImPNG: 'Enregistrer dans un fichier image PNG',
  expImJPG: 'Enregistrer dans un fichier image JPEG'
}
module.exports = textFr
