/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

const textEn = {
  ErrSave: 'An error occured while saving file : ',
  ErrOpen: 'An error occured while opening file : ',
  ExpIm: 'Export your figure in a file',
  ExpHTML: 'Export your figure in a HTML file',
  MtgFiles: 'MathGraph32 Files',
  PNGFiles: 'PNG Files',
  JPGFiles: 'JPEG Files',
  SVGFiles: 'SVG Files',
  HTMLFiles: 'HTML files',
  AllFiles: 'All Files',
  AvertDirty: 'The figure has not been saved. Quit without saving ?',
  SaveFig: 'Save your figure',
  OpenFig: 'Open a mgj figure',
  Choice: 'Choose between :',
  Save: 'Save',
  SaveAs: 'Save as',
  Discard: 'Discard',
  Yes: 'Yes ',
  No: 'No ',
  AddConst: 'Incorporate construction from file',
  SaveProto: 'Saving construction ',
  MGCFiles: 'mgc Files',
  undo: 'Undo',
  redo: 'Redo',
  cut: 'Cut',
  copy: 'Copy',
  paste: 'Paste',
  delete: 'Delete',
  selectall: 'Select all',
  quit: 'Quit',
  file: 'File',
  expImSVG: 'Save in a SVG image file',
  expImPNG: 'Save in a PNG image file',
  expImJPG: 'Save in a JPEG image file'
}
module.exports = textEn
