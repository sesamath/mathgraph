/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

const textEs = {
  ErrSave: 'Ocurrió un error al guardar el archivo : ',
  ErrOpen: 'Ocurrió un error al abrir el archivo : ',
  ExpIm: 'Exportar la figura en un archivo',
  ExpHTML: 'Exportar la figura en un archivo HTML',
  MtgFiles: 'Archivos MathGraph32',
  PNGFiles: 'Archivos PNG',
  JPGFiles: 'Archivos JPEG',
  SVGFiles: 'Archivos SVG',
  HTMLFiles: 'Archivos HTML',
  AllFiles: 'Todos los archivos',
  AvertDirty: 'La figura no ha sido guardada ¿Salir sin guardar?',
  SaveFig: 'Guardar la figura',
  OpenFig: 'Abrir una figura mgj',
  Choice: 'Elija entre :',
  Save: 'Guardar',
  SaveAs: 'Guardar como',
  Discard: 'Anular',
  Yes: 'Si ',
  No: 'No ',
  AddConst: 'Incorporar una construcción desde un archivo',
  SaveProto: 'Guardar la construcción ',
  MGCFiles: 'Archivos mgc',
  undo: 'Anular',
  redo: 'Rehacer',
  cut: 'Cortar',
  copy: 'Copiar ',
  paste: 'Pegar',
  delete: 'Suprimir',
  selectall: 'Seleccionar todo',
  quit: 'Salir',
  file: 'Archivo',
  expImSVG: 'Guardar como archivo de imagen SVG',
  expImPNG: 'Guardar como archivo de imagen PNG',
  expImJPG: 'Guardar como archivo de imagen JPEG'
}
module.exports = textEs
