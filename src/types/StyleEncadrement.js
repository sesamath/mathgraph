/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 * Sert à définir les différents types d'encadrement utilisés dans mtg32
 * @typedef StyleEncadrement
 * @type {Object}
 */
const StyleEncadrement = {
  Sans: 0,
  Simple: 1,
  Effet3D: 2
}

export default StyleEncadrement
