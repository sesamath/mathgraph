/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 * Sert à définir les différnetes positions relatives de deux droites.
 * @typedef PositionDroites
 * @type {Object}
 */
const PositionDroites = {
  Paralleles: 0,
  Confondues: 1,
  Secantes: 2
}

export default PositionDroites
