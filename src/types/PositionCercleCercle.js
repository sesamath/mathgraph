/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 * Sert à définir les différentes positions possibles cercle-cercle.
 * @typedef PositionCercleCercle
 * @type {Object}
 */
const PositionCercleCercle = {
  Confondus: 0,
  Vide: 1,
  Secants: 2,
  Tangents: 3
}

export default PositionCercleCercle
