/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */
/**
 * Sert à définir les différents types de remplissage.
 * @typedef StyleRemplissage
 * @type {Object}
 */
const StyleRemplissage = {
  vertical: 0,
  horizontal: 1,
  haut45: 2,
  bas45: 3,
  plein: 4,
  transp: 5
}
export default StyleRemplissage
