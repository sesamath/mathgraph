/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 * Définition des natures des arcs de cercle.
 * @typedef NatArc
 * @type {Object}
 */
const NatArc = {
  ArcCercle: 0,
  PetitArc: 1,
  GrandArc: 2,
  ArcDirect: 3,
  ArcIndirect: 4
}
export default NatArc
