/*
 * Created by yvesb on 22/10/2016.
 */
/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

import Nat from './Nat'

/**
 * Liste des natures d'objets
 * @typedef NatObj
 * @type {Object}
 */
const NatObj = {
  NAucunObjet: new Nat(0, 0),
  NPointBase: new Nat(1, 0),
  NPoint: new Nat(1 << 1, 0), // 2 puissance1=2
  NPointLie: new Nat(1 << 2, 0),
  NBarycentre: new Nat(1 << 3, 0),
  NPointDansRepere: new Nat(1 << 4, 0),
  NPointBaseEnt: new Nat(1 << 5, 0),
  NPointParAffixe: new Nat(1 << 6, 0),
  NPointLiePoint: new Nat(1 << 7, 0),
  NMacro: new Nat(1 << 8, 0),
  indiceMacro: 8,
  NDroite: new Nat(1 << 9, 0),
  NVecteur: new Nat(1 << 10, 0),
  NSegment: new Nat(1 << 11, 0),
  NDemiDroite: new Nat(1 << 12, 0),
  NCercle: new Nat(1 << 13, 0),
  NMarqueAngle: new Nat(1 << 14, 0),
  NArc: new Nat(1 << 15, 0),
  NPolygone: new Nat(1 << 16, 0),
  NSurface: new Nat(1 << 17, 0),
  NLieu: new Nat(1 << 18, 0),
  NLieuDiscret: new Nat(1 << 19, 0),
  NLigneBrisee: new Nat(1 << 20, 0),
  NLieuObjet: new Nat(1 << 21, 0),
  NImage: new Nat(1 << 22, 0),
  NGrapheSuiteRec: new Nat(1 << 23, 0),
  NGrapheSuiteRecComplexe: new Nat(1 << 24, 0),
  NCommentaire: new Nat(1 << 25, 0),
  NValeurAffichee: new Nat(1 << 26, 0),
  NMarqueSegment: new Nat(1 << 27, 0),
  NObjetDuplique: new Nat(1 << 28, 0),
  NDemiPlan: new Nat(1 << 29, 0),
  // Attention : Dans la version java, le repère est non graphique
  // Il est considéré comme non graphique car dans les constructions il est choisi en premier dans
  // les objets non graphiques
  NRepere: new Nat(1 << 30, 0), // 2^30
  // Version 6.7  On peut remarquer que 2^31 n'a pas été utilisé
  // Version 6.7.2 : Justement il ne faut pas l'utiliser car alors le nombre est considéré comme négatif !
  NTransformation: new Nat(0, 1), // 2^32 JavaScript pas plus de 31 bits pour entier non signé
  NBipoint: new Nat(0, 1 << 1), // 2^33. Pas de décalages de plus de 32 bits en javascript
  NImpProto: new Nat(0, 1 << 2), // 2^34
  NPointInterieurPolygone: new Nat(0, 1 << 3), // 2^35
  NLatex: new Nat(0, 1 << 4), // 2^36
  NPointInterieurCercle: new Nat(0, 1 << 5),
  NEditeurFormule: new Nat(0, 1 << 6), // Ajout version 4.5.2
  /* Version 6.9.1 : Les cinq natures suivantes sont supprimées car inutiles
  NPointClone: new Nat(0, 1 << 7),
  NDroiteClone: new Nat(0, 1 << 8),
  NDemiDroiteClone: new Nat(0, 1 << 9),
  NSegmentClone: new Nat(0, 1 << 10),
  NCercleClone: new Nat(0, 1 << 11),
   */
  // nombreTypesObjetsGraphiques: 47 // Change version 6.9.1
  nombreTypesObjetsGraphiques: 42
}

NatObj.NTtPointSaufPointBase = Nat.or(
  NatObj.NPoint,
  NatObj.NPointLie,
  NatObj.NBarycentre,
  NatObj.NPointDansRepere,
  NatObj.NPointBaseEnt,
  NatObj.NPointParAffixe,
  NatObj.NPointLiePoint,
  NatObj.NPointInterieurPolygone,
  NatObj.NPointInterieurCercle
)
NatObj.NTtPoint = Nat.or(NatObj.NPointBase, NatObj.NTtPointSaufPointBase)
/*
NatObj.NToutPointSaufPointLie = Nat.or(NatObj.NPointBase , NatObj.NPoint , NatObj.NBarycentre ,
  NatObj.NPointDansRepere , NatObj.NPointBaseEnt , NatObj.NPointParAffixe , NatObj.NPointLiePoint ,
  NatObj.NPointInterieurPolygone , NatObj.NPointInterieurCercle);
*/
NatObj.NTtObj = Nat.or(NatObj.NTtPoint, NatObj.NMacro, NatObj.NDroite, NatObj.NVecteur,
  NatObj.NSegment, NatObj.NDemiDroite, NatObj.NCercle
  , NatObj.NMarqueAngle, NatObj.NArc, NatObj.NPolygone, NatObj.NSurface
  , NatObj.NLieu, NatObj.NLieuDiscret, NatObj.NLigneBrisee, NatObj.NLieuObjet, NatObj.NImage
  , NatObj.NGrapheSuiteRec, NatObj.NGrapheSuiteRecComplexe, NatObj.NCommentaire, NatObj.NLatex
  , NatObj.NValeurAffichee, NatObj.NMarqueSegment, NatObj.NObjetDuplique, NatObj.NDemiPlan
  , NatObj.NRepere, NatObj.NEditeurFormule)
NatObj.NTtObjSaufDuplique = Nat.or(NatObj.NTtPoint, NatObj.NMacro, NatObj.NDroite
  , NatObj.NVecteur, NatObj.NSegment, NatObj.NDemiDroite, NatObj.NCercle
  , NatObj.NMarqueAngle, NatObj.NArc, NatObj.NPolygone, NatObj.NSurface, NatObj.NLieu
  , NatObj.NLieuDiscret, NatObj.NLigneBrisee, NatObj.NLieuObjet, NatObj.NImage
  , NatObj.NGrapheSuiteRec, NatObj.NGrapheSuiteRecComplexe, NatObj.NCommentaire, NatObj.NLatex
  , NatObj.NValeurAffichee, NatObj.NMarqueSegment, NatObj.NDemiPlan, NatObj.NRepere
  , NatObj.NEditeurFormule)
NatObj.NTteDroite = Nat.or(NatObj.NDroite, NatObj.NSegment, NatObj.NDemiDroite, NatObj.NVecteur)
NatObj.NTteDroiteSaufVect = Nat.or(NatObj.NDroite, NatObj.NSegment, NatObj.NDemiDroite)
NatObj.NObjLigne = Nat.or(NatObj.NTteDroite, NatObj.NCercle, NatObj.NMarqueAngle
  , NatObj.NArc, NatObj.NPolygone, NatObj.NLieu, NatObj.NLigneBrisee, NatObj.NGrapheSuiteRec
  , NatObj.NGrapheSuiteRecComplexe)
NatObj.NTtCercle = Nat.or(NatObj.NCercle, NatObj.NArc)
NatObj.NObjNommable = Nat.or(NatObj.NTtPoint, NatObj.NDroite)
NatObj.NPointMobile = Nat.or(NatObj.NPointBase, NatObj.NPointLie, NatObj.NPointBaseEnt
  , NatObj.NPointInterieurPolygone, NatObj.NPointInterieurCercle)
NatObj.NAffLieAPoint = Nat.or(NatObj.NCommentaire, NatObj.NLatex, NatObj.NValeurAffichee
  , NatObj.NMacro, NatObj.NImage, NatObj.NEditeurFormule)
NatObj.NAffLieAPointSaufImEd = Nat.or(NatObj.NCommentaire, NatObj.NLatex, NatObj.NValeurAffichee
  , NatObj.NMacro)
NatObj.NTtObjPourLieu = Nat.or(NatObj.NTteDroite, NatObj.NCercle, NatObj.NArc
  , NatObj.NLieu, NatObj.NLieuDiscret, NatObj.NPolygone, NatObj.NLigneBrisee
  , NatObj.NValeurAffichee, NatObj.NCommentaire, NatObj.NLatex
  , NatObj.NTtPointSaufPointBase, NatObj.NLieuObjet)
/*
NatObj.NObjGraphiqueSourcePourConst = Nat.or(NatObj.NTtPoint , NatObj.NSegment , NatObj.NDroite
  , NatObj.NDemiDroite , NatObj.NCercle , NatObj.NArc , NatObj.NPointDansRepere
  , NatObj.NPointLiePoint , NatObj.NVecteur
  , NatObj.NPolygone , NatObj.NLigneBrisee , NatObj.NPointParAffixe , NatObj.NLieu);
*/
NatObj.NPointCapturableNonLie = Nat.or(NatObj.NPointBase, NatObj.NPointBaseEnt,
  NatObj.NPointInterieurCercle, NatObj.NPointInterieurPolygone)
NatObj.NComouLatex = Nat.or(NatObj.NCommentaire, NatObj.NLatex)

// Après le rajout des macros qui sont des objets graphiques, il a
// fallu créer un tableau de conversion qui, à la nature d'un objet
// associe son numéro d'objet graphique, compris entre 0 et
// NombreTypesDesignables - 1

// NatObj.nombreTypesDesignables = 39 // Modifié version 6.9.1
NatObj.nombreTypesDesignables = 34 // Car plus de naure NPointClone, NDroiteClone, NCercleClone, NDemiDroiteClone, NSegmentClone

NatObj.conversionNumeroNature = [
  NatObj.NPointBase, NatObj.NPoint, NatObj.NPointLie, NatObj.NBarycentre,
  NatObj.NPointDansRepere, NatObj.NPointBaseEnt, NatObj.NPointParAffixe,
  NatObj.NPointLiePoint, NatObj.NMacro, NatObj.NDroite, NatObj.NVecteur,
  NatObj.NSegment, NatObj.NDemiDroite, NatObj.NCercle, NatObj.NMarqueAngle,
  NatObj.NArc, NatObj.NPolygone, NatObj.NSurface, NatObj.NLieu,
  NatObj.NLieuDiscret, NatObj.NLigneBrisee, NatObj.NLieuObjet,
  NatObj.NImage, NatObj.NGrapheSuiteRec, NatObj.NGrapheSuiteRecComplexe,
  NatObj.NCommentaire, NatObj.NValeurAffichee, NatObj.NMarqueSegment,
  NatObj.NObjetDuplique, NatObj.NDemiPlan, NatObj.NPointInterieurPolygone,
  NatObj.NLatex, NatObj.NPointInterieurCercle, NatObj.NEditeurFormule]

/* Fonction pas utilisée dans la version C++ renvoyant l'indice de la
 * nature de l'objet dont la nature est nat
 * Pour fonctionner correctement, nat ne doit pas être nul avant appel.
 */

NatObj.indiceDeNature = function (nat) {
  const indnat = nat.indice()
  if (indnat < 30) return indnat
  // A noter qu'il faut retrancher 4 car il n'y a pas d'élement correspondant à Nat{low:31, high:0} c'est-à-dire
  // à 2^31
  else return indnat - 4
}

export default NatObj
