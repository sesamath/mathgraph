/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 * Définition des motifs de points utilisés.
 * @typedef MotifPoint
 * @type Object
 */
const MotifPoint = {
  pave: 0,
  rond: 1,
  croix: 2,
  multi: 3,
  encadre: 4,
  petitRond: 5,
  losange: 6,
  pixel: 7,
  grandRond: 8,
  grandMult: 9,
  quadrillage: 10
}
export default MotifPoint
