/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */

/**
 *
 * @typedef PositionDroiteCercle
 * @type {Object}
 */
const PositionDroiteCercle = {
  Secants: 0,
  Tangents: 1,
  Vide: 2
}
export default PositionDroiteCercle
