Développement mathgraph
=======================

Version installable
-------------------
PWA installable https://developer.mozilla.org/fr/docs/Web/Progressive_web_apps/Guides/Making_PWAs_installable
(Progressive Web App)
et le tuto https://developer.mozilla.org/fr/docs/Web/Progressive_web_apps/Tutorials/CycleTracker

On devrait ainsi gérer le offline sur la version online ordinaire exactement de la même manière.

Appli native
------------

On peut aussi créer une appli native avec react-native et son composant webview (qui peut afficher un html qui charge un js)
https://blog.logrocket.com/react-native-webview-complete-guide/
- commencer par l'appli online ordinaire
- y ajouter l'équivalent des ajouts electron si besoin
