/*
 * Created by yvesb on 29/05/2017.
 */
/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */
import { chaineNombre, getStr } from '../kernel/kernel'
import CLigneBrisee from '../objets/CLigneBrisee'
import NatObj from 'src/types/NatObj'
export default CLigneBrisee

CLigneBrisee.prototype.infoHist = function () {
  return getStr('LigneBrisee') + ' ' + this.nomSommets()
}

CLigneBrisee.prototype.tikz = function (dimf, nomaff, coefmult, bu) {
  let i, x, y
  const list = this.listeProprietaire
  let ch = '\\draw ' + this.tikzLineStyle(dimf, coefmult) + '('
  const nombrePoints = this.nombrePoints
  for (i = 0; i < nombrePoints - 1; i++) {
    x = list.tikzXcoord(this.absPoints[i], dimf, bu)
    y = list.tikzYcoord(this.ordPoints[i], dimf, bu)
    ch += chaineNombre(x, 3) + ',' + chaineNombre(y, 3) + ')--('
  }
  i = nombrePoints - 1
  x = list.tikzXcoord(this.absPoints[i], dimf, bu)
  y = list.tikzYcoord(this.ordPoints[i], dimf, bu)
  ch += chaineNombre(x, 3) + ',' + chaineNombre(y, 3) + ');'
  return ch
}

CLigneBrisee.prototype.coincideAvec = function (p) {
  let ptp1, ptp2, resultat
  if (p.getNature() !== NatObj.NLigneBrisee) return false
  const nombrePoints = this.colPoints.length
  if (nombrePoints !== p.colPoints.length) return false
  // On regarde si la les lignes brisées ont bien les mêmes points dans le même
  // ordre ou dans l'ordre inverse
  resultat = true
  for (let i = 0; i <= nombrePoints - 1; i++) {
    ptp1 = this.colPoints[i].pointeurSurPoint
    ptp2 = p.colPoints[(i)].pointeurSurPoint
    resultat = ptp1.coincideAvec(ptp2)
    if (!resultat) break
  }
  if (resultat) return true
  resultat = true
  for (let i = 0; i <= nombrePoints - 1; i++) {
    ptp1 = this.colPoints[i].pointeurSurPoint
    ptp2 = p.colPoints[(nombrePoints - i - 1)].pointeurSurPoint
    resultat = ptp1.coincideAvec(ptp2)
    if (!resultat) return false
  }
  return true
}
