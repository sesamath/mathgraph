/*
 * Created by yvesb on 23/12/2016.
 */
/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */
import CMediatrice from '../objets/CMediatrice'
import CDroite from '../objets/CDroite'
import { getStr } from '../kernel/kernel'
export default CMediatrice

CMediatrice.prototype.infoHist = function () {
  return this.getName() + ' : ' + getStr('DtMed') + ' ' + getStr('de') + ' [' +
    this.point1.getName() + this.point2.getName() + ']'
}

CMediatrice.prototype.depDe4Rec = function (p) {
  if (this.elementTestePourDependDePourRec === p) return this.dependDeElementTestePourRec
  return this.memDep4Rec(CDroite.prototype.depDe4Rec.call(this, p) ||
    this.point1.depDe4Rec(p) || this.point2.depDe4Rec(p))
}

CMediatrice.prototype.estDefiniParObjDs = function (listeOb) {
  return this.point1.estDefPar(listeOb) &&
  this.point2.estDefPar(listeOb)
}
