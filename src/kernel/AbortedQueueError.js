class AbortedQueueError extends Error {
  // rien à ajouter à une Error ordinaire, cette classe est là simplement pour détecter ce type d'erreur
}

export default AbortedQueueError
