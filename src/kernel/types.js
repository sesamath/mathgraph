// fichier utilisé pour définir des types génériques en jsdoc

/**
 * Une callback qui sera appelée sans argument quand tous les affichages en cours seront terminés (jamais en cas de plantage qq part)
 * @callback DisplayCallback
 */
/**
 * Une fonction de callback qui sera rappelée sans argument
 * @callback VoidCallback
 */
/**
 * Un point (dimension 2) avec propriétés x et y
 * @typedef Point
 * @property {number} x
 * @property {number} y
 */
