import Queue from 'src/kernel/Queue'

// notre queue unique (pour toutes les applis|lecteurs du dom)
let queue

/**
 * Ajoute fn à une pile d'exécution unique (gère un singleton pour garantir l'unicité de la pile)
 * (pile qui sera créée au premier appel)
 * Ça passera à l'élément suivant de la pile quand fn aura terminé (ou sera résolue si c'est une promesse).
 *
 * Si fn() retourne une promesse, ça passera à l'élément suivant de la pile lorsque cette promesse sera résolue ou rejetée.
 *
 * IMPORTANT : on peut faire du addQueue dans fn, ça va remettre du code à la fin de la pile au moment de l'exécution de fn,
 * mais il ne faut surtout pas faire dans fn du `return addQueue(autreFct)` car cette promesse ne serait jamais résolue !
 * (il faut attendre la résolution du premier `addQueue(fn)` avant de passer à l'élément suivant, si cette
 * résolution dépend de l'exécution d'un élément plus loin sur la pile elle n'arrivera jamais)
 * @param {function|Promise} fn
 * @param [doNotCatch] passer true pour que l'erreur soit retournée (promesse échouée) plutôt que gérée par la pile
 */
export function addQueue (fn, doNotCatch) {
  if (typeof fn !== 'function') throw TypeError('il faut passer une fonction')
  if (!queue) queue = new Queue()
  return queue.add(fn, doNotCatch)
}

/**
 * Annule la pile courante et en génère une nouvelle
 * @todo gérer une pile par MtgApp ou doc de MtgAppLecteur, pour éviter qu'un abort() ne touche toutes les figures en cours d'affichage (ou chargement).
 */
export function abort () {
  queue.abort()
  queue = new Queue()
}

export default addQueue
