import AbortedQueueError from './AbortedQueueError'

/**
 * Pile d'exécution de fonctions, qui seront lancée en séquentiel, qu'elles soient sync ou async
 */
class Queue {
  constructor () {
    /**
     * La pile de promesse
     * @type {Promise<void>}
     * @private
     */
    this._stack = Promise.resolve()
    /**
     * Un flag pour savoir si la pile a été annulée
     * @type {boolean}
     * @private
     */
    this._aborted = false
  }

  /**
   * Ajoute une fonction à la pile d'exécution
   * @param {function|Promise} fn
   * @param [doNotCatch] passer true pour que l'erreur soit retournée (promesse échouée) plutôt que gérée ici
   * @this {MtgApp}
   * @returns {Promise<any>} qui sera résolue avec la valeur retournée par fn (ou undefined en cas de plantage de fn géré ici)
   */
  add (fn, doNotCatch) {
    if (fn instanceof Promise) fn = () => fn
    if (typeof fn !== 'function') throw TypeError('Queue ne gère que des fonctions ou des Promise')
    // si la pile est aborted, il fauth throw pour que le code parent soit informé
    // (sinon il utilise la valeur de résolution, que serait undefined, et continuerait comme si de rien n'était)
    if (this._aborted) throw new AbortedQueueError('Task added in aborted queue => canceled')

    // on enrobe fn pour gérer _aborted
    const step = () => {
      if (this._aborted) throw new AbortedQueueError('Current queue was aborted since this task was added => task canceled')
      return fn()
    }

    if (doNotCatch) {
      // on va retourner une promesse éventuellement rejetée, mais faut pas garder ce rejet
      // dans notre pile sinon on pourra plus rien ajouter ensuite
      const promise = this._stack.then(step)
      this._stack = promise.catch(() => undefined) // on drop l'erreur puisque l'appelant est sensé la gérer
      return promise
    }
    // sinon on empile et retourne le tout
    this._stack = this._stack.then(step).catch(error => console.error(error))
    return this._stack
  }

  /**
   * Annule la pile courante (qui ne pourra plus être utilisée)
   */
  abort () {
    this._aborted = true
  }
}

export default Queue
