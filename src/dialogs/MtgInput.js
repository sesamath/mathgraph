/*
 * Created by yvesb on 04/04/2017.
 */
/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */
import { ce } from '../kernel/kernel'
import $ from 'jquery'

export default MtgInput

/**
 *
 * @param arg
 * @returns {Element}
 * @constructor
 */
function MtgInput (arg) {
  const argt = arguments.length === 0 ? {} : arguments[0]
  argt.type = 'text'
  const inp = ce('input', argt)
  inp.setAttribute('spellcheck', 'false')
  inp.isRed = false
  inp.onblur = function () {
    if (inp.isRed) $(inp).css('background-color', '#FFFFFF')
  }
  inp.marquePourErreur = function () {
    $(inp).css('background-color', '#F5a9BC')
    inp.isRed = true
  }
  inp.demarquePourErreur = function () {
    if (inp.isRed) $(inp).css('background-color', '#FFFFFF')
    inp.isRed = false
  }
  inp.onkeyup = function () {
    if (inp.isRed) $(inp).css('background-color', '#FFFFFF')
    inp.isRed = false
  }
  // this.input = inp;
  return inp
}

/*
MtgInput.prototype.marquePourErreur = function() {
  $(this.input).css("background-color", "#FF9999");
}
  */
