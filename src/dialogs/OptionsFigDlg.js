/*
 * Created by yvesb on 16/04/2017.
 */
import ShortcutsDlg from './ShortcutsDlg'

/*
 * MathGraph32 Javascript : Software for animating online dynamic mathematics figures
 * https://www.mathgraph32.org/
 * @Author Yves Biton (yves.biton@sesamath.net)
 * @License: GNU AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 */
import { addZoomListener, ce, ceIn, getStr, mtgFileExtension, uniteAngleDegre, uniteAngleRadian } from '../kernel/kernel'
import { getButtonArrow } from '../kernel/kernelAdd'
import MtgDlg from './MtgDlg'
import OptionsAnimDlg from './OptionsAnimDlg'
import ImageFondDlg from './ImageFondDlg'
import ConfirmDlg from './ConfirmDlg'
import ChoixOutilsDlg from './ChoixOutilsDlg'
import $ from 'jquery'
import CMathGraphDoc from '../objetsAdd/CMathGraphDocAdd'
import MtgInput from './MtgInput'
import EditeurConst from './EditeurConst'
import AboutDlg from './AboutDlg'
import TextFigDlg from './TextFigDlg'
import ChoixLangDlg from './ChoixLangDlg'
import CoulFondDlg from './CouleurFondDlg'
import ChoixEltsFigesDlg from './ChoixEltsFigesDlg'
import ChoixMacDemDlg from './ChoixMacDemDlg'
import AvertDlg from 'src/dialogs/AvertDlg'
import DataInputStream from 'src/entreesSorties/DataInputStream'
export default OptionsFigDlg

/**
 *
 * @param {MtgApp} app
 * @param callBackOK
 * @constructor
 */
function OptionsFigDlg (app, callBackOK) {
  MtgDlg.call(this, app, 'OptionsFigDlg', callBackOK)
  let cb, tr, td, label
  const self = this
  /// ////////////////////////////////////////////////////////////////////////////////////////////////
  // On crée un nouveau document pour stocker les choix d'outils au cas où l'utilisateur sortirait de la
  // boîte de dialogue sans valider
  this.doc = new CMathGraphDoc('doc')
  this.doc.setIdMenusFromDoc(app.doc)
  /// ////////////////////////////////////////////////////////////////////////////////////////////////

  const list = app.listePr
  // On cherche d'abord si on utilise ou non un niveau prédéfini
  let niv
  for (niv = 0; niv < 4; niv++) {
    if (app.level === app.levels[niv]) break
  }
  // Test suivant car on peut avoir chargé un niveau personnalisé
  if (niv < 4) this.niv = niv; else {
    this.niv = 4
    this.personalizedLevel = true
  }
  // this.niv vaut 4 si la figure utilise une figure spécifique pour le niveau d'utilisation -fonctionnement personnalisé)
  // Dans ce cas on ne donne pas la possibilité de changer de niveau d'utilisation
  //
  const tabPrincipal = ce('table')
  this.appendChild(tabPrincipal)
  tr = ce('tr')
  tabPrincipal.appendChild(tr)

  // unité d'angle
  const tabUnite = ce('table')
  tr.appendChild(tabUnite)
  tr = ce('tr')
  tabUnite.appendChild(tr)
  let div = ce('div')
  tr.appendChild(div)
  label = ce('label')
  $(label).html(getStr('unAng'))
  div.appendChild(label)
  const radioDegre = ce('input', {
    type: 'radio',
    id: 'degre',
    name: 'unite'
  })
  div.appendChild(radioDegre)
  if (list.uniteAngle === uniteAngleDegre) $('#degre').attr('checked', 'checked')
  label = ce('label', {
    for: 'degre'
  })
  $(label).html(getStr('Degre'))
  div.appendChild(label)
  label = ce('label', {
    for: 'radian'
  })
  const radioRadian = ce('input', {
    type: 'radio',
    id: 'radian',
    name: 'unite',
    style: 'margin-left:15px'
  })
  div.appendChild(radioRadian)
  if (list.uniteAngle === uniteAngleRadian) $('#radian').attr('checked', 'checked')
  $(label).html(getStr('Radian'))
  div.appendChild(label)

  // Une checkBox pour savoir si on utilise le . ou la virgule comme séparateur décimal.
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  cb = ce('input', {
    type: 'checkbox',
    id: 'cbDecimalDot'
  })
  tr.appendChild(cb)
  label = ce('label', {
    for: 'cbDecimalDot'
  })
  $(label).html(getStr('decimalDot'))
  tr.appendChild(label)
  if (app.decimalDot) $(cb).attr('checked', 'checked')
  // /////////////////////////////////////////////////////////////////
  // Quatre boutons radio pour le choix du niveau d'id niv0 à niv3  //
  // Et un de plus si on est en fonctionnement personnalisé         //
  // /////////////////////////////////////////////////////////////////
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  const tabNiveau = ce('table')
  tr.appendChild(tabNiveau)
  const nivMax = this.personalizedLevel ? 4 : 3
  for (niv = 0; niv <= nivMax; niv++) {
    if (niv % 2 === 0) {
      tr = ce('tr')
      tabNiveau.appendChild(tr)
    }
    td = ce('td')
    tr.appendChild(td)
    const rad = ce('input', {
      type: 'radio',
      id: 'niv' + niv,
      name: 'niv'
    })
    td.appendChild(rad)
    if (this.niv === niv) $(rad).attr('checked', 'checked')
    label = ce('label', {
      for: 'niv' + niv
    })
    $(label).html(getStr('niv' + niv))
    td.appendChild(label)
  }

  if (app.electron || app.pwa) {
    // Pour la version electron on propose de redémarrer avec le niveau choisi
    tr = ce('tr')
    tabPrincipal.appendChild(tr)
    div = ce('div')
    tr.appendChild(div)
    cb = ce('input', {
      type: 'checkbox',
      id: 'cbstartlevel'
    })
    div.appendChild(cb)
    label = ce('label', {
      for: 'cbstartlevel'
    })
    $(label).html(getStr('NivDem'))
    div.appendChild(label)
    // Pour la version electron on propose de redémarrer en mode dys
    tr = ce('tr')
    tabPrincipal.appendChild(tr)
    div = ce('div')
    tr.appendChild(div)
    cb = ce('input', {
      type: 'checkbox',
      id: 'cbdys'
    })
    div.appendChild(cb)
    label = ce('label', {
      for: 'cbdys'
    })
    $(label).html(getStr('DysDem'))
    div.appendChild(label)
    if (app.dys) $(cb).attr('checked', 'checked')
    // Pour la version electron on peut choisir la figure de démarrage. 3 choix possibles
    tr = ce('tr')
    tabPrincipal.appendChild(tr)
    const tabStart = ce('table')
    tr.appendChild(tabStart)
    tr = ce('tr')
    tabStart.appendChild(tr)
    td = ce('td')
    tr.appendChild(td)
    label = ce('label')
    $(label).html(getStr('start'))
    td.appendChild(label)
    const choix = ['frameGrid', 'frameDotted', 'unity']
    for (let i = 0; i < 3; i++) {
      td = ce('td')
      tr.appendChild(td)
      const rd = ce('input', {
        type: 'radio',
        id: 'rdst' + i,
        name: 'start'
      })
      if (app.pref_StartFig === choix[i]) $(rd).attr('checked', 'checked')
      td.appendChild(rd)
      label = ce('label')
      $(label).html(getStr('start' + i))
      td.appendChild(label)
    }
  }

  /// /////////////////////////////////////////////////////////////////////////////////////////////
  // Une checkBox pour personnaliser les outils disponibles. Si un choix a déjà été fait        //
  // cette case est cochée et le bouton de choix des outils est visible.                        //
  // Sinon le bouton de choix des outils n'apparait que si on coche la case                     //
  // Toute action sur la checkBox réinitialise les outils à aucun outil interdit                //
  /// /////////////////////////////////////////////////////////////////////////////////////////////
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div')
  tr.appendChild(div)
  cb = ce('input', {
    type: 'checkbox',
    id: 'cbtools'
  })
  if (app.doc.hasOwnTools()) $(cb).attr('checked', 'checked')
  div.appendChild(cb)
  cb.onclick = function () {
    const checked = $('#cbtools').prop('checked')
    $('#div').css('display', checked ? 'inline' : 'none')
    if (!checked) self.doc.setIdMenus(app, true, [], 3)
  }
  label = ce('label', {
    for: 'cbtools'
  })
  $(label).html(getStr('OutilsPer'))
  div.appendChild(label)
  /// /////////////////////////////////////////////////////////////////////////////////////////////
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div', {
    id: 'div'
  })
  tr.appendChild(div)
  // var btnTools = getButtonArrow(app, "tools");
  const btnTools = ce('button', {
    style: 'margin-left:20px;'
  })
  $(btnTools).html(getStr('ChoixOutils'))
  btnTools.onclick = function () {
    // Modification version 7.7.5 : Le niveau doit être 3 si on a chargé une figure avec un niveau personnalisé
    // new ChoixOutilsDlg(self.app, self.doc, self.getSelectedNiv())
    const selectedNiv = self.getSelectedNiv()
    const niv = selectedNiv === 4 ? 3 : selectedNiv
    new ChoixOutilsDlg(self.app, self.doc, niv, null, !$('#cbstartlevel').prop('checked'))
  }
  div.appendChild(btnTools)
  $(div).css('display', app.doc.hasOwnTools() ? 'inline' : 'none')

  // Un bouton pour choisir les outils permis ou interdits depuis un fichier contenant une figure
  const input = ce('input', {
    type: 'file',
    id: 'file',
    accept: '.' + mtgFileExtension,
    class: 'inputfile'
  })
  div.appendChild(input)
  input.addEventListener('change', function () {
    const file = input.files[0]
    if (file) {
      const reader = new FileReader()
      reader.readAsArrayBuffer(file)
      reader.onload = function () {
        try {
          const ba = new Uint8Array(reader.result)
          let doc
          try {
            const inps = new DataInputStream(ba)
            // var doc = new CMathGraphDoc(self.id + "figurePanel", true, true); // Modifié version 6.3.0
            doc = new CMathGraphDoc(app.id, true, true)
            doc.read(inps)
            new ChoixOutilsDlg(app, self.doc, 3, doc, !$('#cbstartlevel').prop('checked')) // 3 pour utiliser le niveau le plus avancé
          } catch (e) {
            new AvertDlg(app, e.message)
          }
        } catch (e) {
          new AvertDlg(app, e.message)
        }
        input.value = null
      }
    }
  })
  const fileLabel = ce('label', {
    for: 'file'
  })
  fileLabel.style.marginLeft = '30px'
  $(fileLabel).html(getStr('ChoixOutilsFig'))
  div.appendChild(fileLabel)

  /// ////////////////////////////////////////////////////////////////////////////////////////////////
  // Une checkBox pour afficher ou non directement les mesures de longueur et d'angle sur la figure
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div')
  tr.appendChild(div)
  cb = ce('input', {
    type: 'checkbox',
    id: 'cbmes'
  })
  if (app.displayMeasures) $(cb).attr('checked', 'checked')
  div.appendChild(cb)
  label = ce('label', {
    for: 'cbmes'
  })
  $(label).html(getStr('DispMeas'))
  div.appendChild(label)

  /// /////////////////////////////////////////////////////////////////////////////////////////////
  // Une checkBox pour donner la possibilité d'afficher un cadre grisé de dimensions données
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div')
  tr.appendChild(div)
  cb = ce('input', {
    type: 'checkbox',
    id: 'cbcadre'
  })
  div.appendChild(cb)
  cb.onclick = function () {
    self.onClickDim()
  }
  if (app.cadre !== null) $('#cbcadre').attr('checked', 'checked')
  label = ce('label', {
    for: 'cbcadre'
  })
  $(label).html(getStr('AffCad'))
  div.appendChild(label)

  // Dessous deux champs d'édition pour largeur et hauteur du cadre désiré
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div', {
    id: 'divdim'
  })
  tr.appendChild(div)
  label = ce('label', {
    style: 'margin-left: 20px;'
  })
  $(label).html(getStr('Larg') + ' : ')
  div.appendChild(label)
  const w = Math.floor(app.dimf.x)
  const inputw = new MtgInput({
    id: 'inpw',
    size: 4
  })
  div.appendChild(inputw)
  if (app.cadre !== null) $(inputw).val(app.widthCadre)
  label = ce('label')
  $(label).css('font-size', '13px')
  $(label).html('(10 ' + getStr('a') + ' ' + w + ')')
  div.appendChild(label)

  label = ce('label', {
    style: 'margin-left: 20px;'
  })
  $(label).html(getStr('Haut') + ' : ')
  div.appendChild(label)
  const h = Math.floor(app.dimf.y)
  const inputh = new MtgInput({
    id: 'inph',
    size: 4
  })
  div.appendChild(inputh)
  if (app.cadre !== null) $(inputh).val(app.heightCadre)
  label = ce('label')
  $(label).css('font-size', '13px')
  $(label).html('(10 ' + getStr('a') + ' ' + h + ')')
  div.appendChild(label)

  // On donne la possibilité d'activer ou pas le zoom avec la roulette de la souris
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  cb = ce('input', {
    type: 'checkbox',
    id: 'cbwheel'
  })
  tr.appendChild(cb)
  if (app.zoomOnWheel) $('#cbwheel').attr('checked', 'checked')
  label = ce('label', {
    for: 'cbwheel'
  })
  $(label).html(getStr('ZoomOnWheel'))
  tr.appendChild(label)

  /// /////////////////////////////////////////////////////////////////////////////////////////////
  // Une checkBox pour montrer ou cacher l'image de fond s'il y en a une
  if (app.doc.imageFond !== null) {
    tr = ce('tr')
    tabPrincipal.appendChild(tr)
    div = ce('div')
    tr.appendChild(div)
    cb = ce('input', {
      type: 'checkbox',
      id: 'cbimfvis'
    })
    div.appendChild(cb)
    if (app.doc.imageFondVisible) $(cb).attr('checked', true)
    cb.onclick = function () {
      const b = $('#cbimfvis').prop('checked')
      $(self.app.gImageFond).css('visibility', b ? 'visible' : 'hidden')
      self.app.doc.imageFondVisible = b
    }
    label = ce('label', {
      for: 'cbimfvis'
    })
    $(label).html(getStr('ImFondVis'))
    div.appendChild(label)
  }

  // coef multiplicateur pour l'export des images
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  div = ce('div')
  tr.appendChild(div)
  label = ce('label')
  $(label).html(getStr('coefMult'))
  div.appendChild(label)
  const inputCoef = new MtgInput({
    id: 'inpcoef',
    size: 4
  })
  div.appendChild(inputCoef)
  $('#inpcoef').val(app.pref_coefMult)

  this.editw = new EditeurConst(app, inputw, 10, w)
  this.edith = new EditeurConst(app, inputh, 10, h)
  if (app.cadre === null) this.onClickDim()

  this.editcoef = new EditeurConst(app, inputCoef, 0.25, 4, false)

  /// /////////////////////////////////////////////////////////////////////////////////////////////
  // En bas on rajoute des items avec des flèches pour ouvrir d'autres boîtes de dialogue
  tr = ce('tr')
  tabPrincipal.appendChild(tr)
  const tabBas = ce('table')
  tr.appendChild(tabBas)

  tr = ce('tr')
  tabBas.appendChild(tr)
  let btn = getButtonArrow(app, 'figes')
  btn.onclick = function () {
    new ChoixEltsFigesDlg(self.app)
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'figes'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('EltsFiges'))
  tr.appendChild(label)

  tr = ce('tr')
  tabBas.appendChild(tr)
  btn = getButtonArrow(app, 'anim')
  btn.onclick = function () {
    new OptionsAnimDlg(self.app, null)
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'anim'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('OptionsAnim'))
  tr.appendChild(label)

  if (app.doc.imageFond === null) {
    tr = ce('tr')
    tabBas.appendChild(tr)
    btn = getButtonArrow(app, 'imFond')
    btn.onclick = function () {
      new ImageFondDlg(self.app, function () {
        self.destroy()
      })
    }
    tr.appendChild(btn)
    label = ce('label', {
      class: 'labelarrow',
      for: 'imFond'// Pour qu'on puisse activer l'outil en cliquant sur le label
    })
    $(label).html(getStr('ChoixImFond'))
    tr.appendChild(label)
  } else {
    tr = ce('tr')
    tabBas.appendChild(tr)
    btn = getButtonArrow(app, 'imFond')
    btn.onclick = function () {
      new ConfirmDlg(self.app, 'AvertNonAnnul', function () {
        self.app.deleteImageFond()
        self.destroy()
      })
    }
    tr.appendChild(btn)
    label = ce('label', {
      class: 'labelarrow',
      for: 'imFond'// Pour qu'on puisse activer l'outil en cliquant sur le label
    })
    $(label).html(getStr('DelImFond'))
    tr.appendChild(label)
  }

  tr = ce('tr')
  tabBas.appendChild(tr)
  btn = getButtonArrow(app, 'couFond')
  btn.onclick = function () {
    new CoulFondDlg(self.app, function () {
      self.destroy()
    })
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'couFond'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('CouFond'))
  tr.appendChild(label)

  // la langue
  tr = ce('tr')
  tabBas.appendChild(tr)
  btn = getButtonArrow(app, 'choixL')
  btn.onclick = function () {
    new ChoixLangDlg(self.app)
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'choixL'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('ChoixLang'))
  tr.appendChild(label)

  if (list.listeMacPourDem().noms.length > 1) {
    tr = ce('tr')
    tabBas.appendChild(tr)
    btn = getButtonArrow(app, 'macdem')
    btn.onclick = function () {
      new ChoixMacDemDlg(self.app)
    }
    tr.appendChild(btn)
    label = ce('label', {
      class: 'labelarrow',
      for: 'macdem'// Pour qu'on puisse activer l'outil en cliquant sur le label
    })
    $(label).html(getStr('ChoixMacDem'))
    tr.appendChild(label)
  }

  tr = ce('tr')
  tabBas.appendChild(tr)
  btn = getButtonArrow(app, 'TextFig')
  btn.onclick = function () {
    new TextFigDlg(self.app)
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'TextFig'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('TextFig'))
  tr.appendChild(label)

  // bouton à propos
  tr = ce('tr')
  tabBas.appendChild(tr)
  btn = getButtonArrow(app, 'about')
  btn.onclick = function () {
    new AboutDlg(self.app)
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'about'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('About'))
  tr.appendChild(label)

  // raccourcis clavier
  tr = ceIn(tabBas, 'tr')
  td = ceIn(tr, 'td')
  btn = getButtonArrow(app, 'shortcuts')
  btn.onclick = function () {
    new ShortcutsDlg(self.app)
  }
  td.appendChild(btn)
  label = ceIn(td, 'label', {
    class: 'labelarrow',
    for: 'shortcuts' // Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  label.innerHTML = getStr('Shortcuts')

  // lien vers le site
  tr = ceIn(tabBas, 'tr')
  btn = getButtonArrow(app, 'Web')
  btn.onclick = function () {
    window.open('https://www.mathgraph32.org')
  }
  tr.appendChild(btn)
  label = ce('label', {
    class: 'labelarrow',
    for: 'Web'// Pour qu'on puisse activer l'outil en cliquant sur le label
  })
  $(label).html(getStr('Web'))
  tr.appendChild(label)

  // Création de la boîte de dialogue par jqueryui
  this.create('OptionsFig', 700)
}

OptionsFigDlg.prototype = new MtgDlg()

OptionsFigDlg.prototype.onClickDim = function () {
  const app = this.app
  $('#divdim').css('display', $('#cbcadre').prop('checked') ? 'inline' : 'none')
  // On soustrait 20 pour pouvoir bien capturer le cadre au touch
  $('#inpw').val(Math.floor(app.dimf.x) - 40)
  $('#inph').val(Math.floor(app.dimf.y) - 40)
}

OptionsFigDlg.prototype.OK = function () {
  const app = this.app
  const electron = app.electron
  const list = app.listePr
  const decDot = $('#cbDecimalDot').prop('checked')
  app.decimalDot = decDot
  list.decimalDot = decDot
  const sel = $('#cbcadre')
  if ((sel.prop('checked') ? (this.editw.validate() && this.edith.validate()) : true) && this.editcoef.validate()) {
    list.uniteAngle = $('#radian').prop('checked') ? uniteAngleRadian : uniteAngleDegre
    // On regarde le niveau choisi est le même que le niveau précédent
    const niv = this.getSelectedNiv()
    // Pour annuler un fonctionnement personnalisé, il faut soit cocher cocher la case utiliser ce niveau aup prochain démmarrage
    // soit choisir de nouveaux outils en cochant la case Utiliser ce choix d'outil au prochain démarrage
    // Si  le logiciel est en mode de fonctionnement personnalisé et si on a coché la case d'un niveau prédéfini,
    // on va changer les icônes relativement au niveau coché mais cela est provisoire saut si on a coché
    // la case Utiliser le niveau choisipour le prochain démarrage
    if (niv !== 4) {
      app.levelIndex = niv // Utilisé dans getResult()
      app.level = app.levels[niv]
    }
    app.doc.setIdMenusFromDoc(this.doc)
    // On met à jour les icônes de la barre horizontale suibant le document
    app.updateToolbar()
    // }
    // Pour la version electron si la case est cochéee on appelle setLevel de la page index.html de façon que
    // le niveau soit mémorisé par l'application
    // et idem pour le redémarrage en mode dys
    // Idem pour l'affichage des mesures sur la figure
    if (electron || app.pwa) {
      const choix = ['frameGrid', 'frameDotted', 'unity']
      // Si on est déjà en niveau de fonctionnement personnalisé et si on a coché la case
      // Utiliser ce choix pour le démarrage du logiciel il n'y a pas à appeler setLevel(niv)
      if ($('#cbstartlevel').prop('checked') && (niv < 4)) {
        // eslint-disable-next-line no-undef
        if (electron) setLevel(niv)
        else localStorage.setItem('level', niv.toString())
      }
      // setLevel est défini dans index.html pour electron
      const checked = $('#cbdys').prop('checked')
      if (electron) setDysMode(checked) // eslint-disable-line no-undef
      else localStorage.setItem('dysmode', checked.toString())
      // setDecimalDot est défini dans index.html pour electron
      if (electron) setDecimalDot(decDot) // eslint-disable-line no-undef
      else localStorage.setItem('decimalDot', decDot.toString())
      // setDysMode est défini dans index.html
      const dispm = $('#cbmes').prop('checked')
      if (electron) setDisplayMeasures(dispm) // eslint-disable-line no-undef
      else localStorage.setItem('displaymeasures', dispm.toString())
      // setDisplayMeasures est défini dans index.html pour la version electron
      let i
      for (i = 0; i < 3; i++) if ($('#rdst' + i).prop('checked')) break
      if (electron) setStartFig(choix[i]) // eslint-disable-line no-undef
      else localStorage.setItem('startFig', choix[i])
      // setStartFig est défini dans index.html pour la version electron
    }
    if (app.cadre !== null) app.deleteCadre()
    if (sel.prop('checked')) {
      const wr = parseInt($('#inpw').val())
      const hr = parseInt($('#inph').val())
      app.createCadre(wr, hr)
    }
    if ($('#cbwheel').prop('checked')) {
      if (!app.doc.wheelListener) {
        addZoomListener(app.doc, app.svgFigure, app.svgGlob)
      }
      app.zoomOnWheel = true
    } else {
      if (app.doc.wheelListener) {
        app.svgGlob.removeEventListener('wheel', app.doc.wheelListener)
        app.doc.wheelListener = null
      }
      app.zoomOnWheel = false
    }
    // eslint-disable-next-line no-undef
    if (electron) setZoomOnWheel(app.zoomOnWheel)
    else {
      if (app.pwa) localStorage.setItem('zoomOnWheel', app.zoomOnWheel.toString())
    }
    app.pref_coefMult = parseFloat($('#inpcoef').val())
    app.displayMeasures = $('#cbmes').prop('checked')
    app.updateToolsToolBar()
    app.activeOutilCapt()
    this.callBackOK()
    this.destroy()
  }
}

OptionsFigDlg.prototype.getSelectedNiv = function () {
  const nivMax = this.personalizedLevel ? 4 : 3
  for (let niv = 0; niv <= nivMax; niv++) {
    if ($('#niv' + niv).prop('checked')) return niv
  }
}
