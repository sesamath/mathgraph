/*
 * Created by yvesb on 28/09/2016.
 */
import CMarqueAngleOrienteDirecte from '../objets/CMarqueAngleOrienteDirecte'
import OutilObjetPar3Pts from './OutilObjetPar3Pts'
export default OutilMarqueAngOrSD
/**
 * Outil servant à créer une bissectrice
 * @param {MtgApp} app L'application propriétaire
 * @constructor
 */
function OutilMarqueAngOrSD (app) {
  OutilObjetPar3Pts.call(this, app, 'MarqueAngOrSD', 33047, true)
}

OutilMarqueAngOrSD.prototype = new OutilObjetPar3Pts()

// Opacité par défaut de 0.2 quand on crée une marque d'angle
OutilMarqueAngOrSD.prototype.isSurfTool = function () {
  return true
}

OutilMarqueAngOrSD.prototype.objet = function (pt1, pt2, pt3) {
  const app = this.app
  return new CMarqueAngleOrienteDirecte(app.listePr, null, false, app.getCouleur(), false, app.getStyleTrait(),
    app.getStyleMarqueAngle(), 16, pt1, pt2, pt3, app.getStyleFleche())
}

OutilMarqueAngOrSD.prototype.preIndication = function () {
  return 'MarqueAngOrSD'
}
