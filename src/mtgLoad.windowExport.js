// depuis webpack 5, plus moyen d'exporter mtgLoad en global (ça marchait bien en v4),
// même en suivant à la lettre la doc https://webpack.js.org/configuration/output/#outputlibraryexport
// avec par ex
// const config = {
//   entry: {
//     mtgLoad: './src/mtgLoad.js'
//   },
//   output: {
//     library: {
//       name: '[name]',
//       type: 'var',
//       export: 'default'
//     }
//   }
// }

// => on va pas y passer des heures, de toute manière mathgraph n'est utilisable que dans un navigateur
// et si qqun veut l'inclure dans son propre build sans l'exporter en global il a juste à
// - importer notre dépôt dans ses dépendances avec
//    "mathgraph": "git+ssh://git@src.sesamath.net:mathgraph_js"
// - importer dans son fichier js avec
//    import mtgLoad from 'mathgraph'

import mtgLoad from './mtgLoad'

window.mtgLoad = mtgLoad
