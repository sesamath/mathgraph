export const fig1 = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAABAAAABgAPQW5nbGVfQWZmaWNoYWdlANVGb3Vybml0IHVuIGFuZ2xlIHBlcm1ldHRhbnQgZW5zdWl0ZSBkJ2FsaWduZXIgdW4gYWZmaWNoYWdlIGF2ZWMgdW4gc2VnbWVudCBlbiBjbGlxdWFudCBzdXIgZGV1eCBwb2ludHMuCiMxOmxlIHBvaW50IEEgKHBvdXIgdW4gYWZmaWNoYWdlIG9yaWVudMOpIGRlIEEgdmVycyBCKQojMjpsZSBwb2ludCBCIChwb3VyIHVuIGFmZmljaGFnZSBvcmllbnTDqSBkZSBBIHZlcnMgQikAAAACAAAAAQAAAAAL#####wAAAAEAEUNFbGVtZW50R2VuZXJpcXVlAAAAAAAA#####wAAAAAAAAAAAAD##########wAAAAEAC0NQb2ludENsb25lAP####8AAAAAABAAAlcxAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAAAAAAEA#####wAAAAAAEAACVzIAAAAAAAAAAABACAAAAAAAAAAABQAAAAAB#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8AAAAAARAAAAEAAAABAAAAAAE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wAAAAAAEAACVzMAAAAAAAAAAABACAAAAAAAAAAABQABQGagAAAAAAAAAAAE#####wAAAAIAE0NNZXN1cmVBbmdsZU9yaWVudGUA#####wAGTWVzQW5nAAAABQAAAAIAAAAD#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAF#####wAAAAEAC0NQb2ludEltYWdlAP####8AAAAAABAAAlc0AAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAgAAAAcAAAAEAP####8ABGZsYXQAAAACAAAABQAAAAj#####AAAAAQAHQ0NhbGN1bAH#####AAhBbmdsZUFmZgApc2koYWJzKE1lc0FuZyk+ZmxhdC8yLGZsYXQrTWVzQW5nLE1lc0FuZyn#####AAAAAQANQ0ZvbmN0aW9uM1ZhcgD#####AAAAAQAKQ09wZXJhdGlvbgX#####AAAAAgAJQ0ZvbmN0aW9uAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAGAAAACQMAAAALAAAACf####8AAAABAApDQ29uc3RhbnRlQAAAAAAAAAAAAAAJAAAAAAsAAAAJAAAACwAAAAYAAAALAAAABgAAAAEAAAAl#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDYAAAAMQAkh+1RELRj#####AAAAAQAPQ1ZhcmlhYmxlQm9ybmVlAP####8AAXVAJAAAAAAAAAAAAAAAAAAAQFkAAAAAAABAJAAAAAAAAAEAATAAAzEwMAACMTD#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAyOJN0vGp#QDI4k3S8an8AAAACAP####8BAAAAABAAAAEAAAABAAAAAgE#8AAAAAAAAAAAAAMA#####wAAAAAADgABVgDAAAAAAAAAAEAQAAAAAAAAAAAFAAFAQjiTdLxqfwAAAAP#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAEAAAAQAAAAEAAAACAAAABP####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAIAAAAE#####wAAAAIADENDb21tZW50YWlyZQD#####AAAAAAAAAAAAAAAAAEAYAAAAAAAAAAAAAAAGDAAAAAAAAQAAAAAAAAAMAAAAAAAAAAAAATH#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAACAAAABAAAAAcA#####wADYW5nAAMyMDAAAAAMQGkAAAAAAAAAAAAPAP####8AAAAAABAAAU8AAAAAAAAAAABACAAAAAAAAAAABQABQHggAAAAAABAc+1wo9cKPgAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUCC6AAAAAAAQGm64UeuFHv#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0AP####8AAAAAAAAAAQAAAAoAAAAL#####wAAAAsAAAAJ#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP6F+cyNUfx4AAAAM#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQD#####AAAA#wAAAAAADQAAAAxAWQAAAAAAAAAAAA0AAAACAAAADQAAAA3#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAKAAAACQIAAAAJAwAAAAsAAAAJAAAADEBZAAAAAAAAAAAACwAAAAEAAAAGAP####8A#wAAARAAAlcnAAAAAAAAAAAAQAgAAAAAAAAAAAEAAAAACwAAAA8AAAACAP####8AAAAAARAAAAEAAAEBAAAACgE#8AAAAAAAAAAAAAMA#####wAAAAABEAABVwAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAZaAAAAAAAAAAABEAAAAEAP####8ABmFuZ2hvcgAAABIAAAAKAAAAEP####8AAAACABVDTGlldU9iamV0UGFyVmFyaWFibGUA#####wD#AAAAAAAAABAAAAAMQCYAAAAAAAAAAAABAAAAAwAAAAEAAAAPAAAAEP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8AAAAAAA0AAAEAAAEBAAAACgAAABD#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAABAQAAABAAAAAMQDQAAAAAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAABUAAAAW#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAABcAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAX#####wAAAAIABkNMYXRleAD#####AP8AAAEAAAAAABkQAAAAAAABAAAAAQAAAAkBAAAACwAAABMAAAAMQFaAAAAAAAAAB1xWYWx7dX0AAAAYAP####8A#wAAAAAAAAAaAAAADEAmAAAAAAAAAAAAAQAAAAkAAAABAAAADwAAABAAAAATAAAAFQAAABYAAAAXAAAAGQAAABoAAAADAP####8AAAAAABAAAUkAQAgAAAAAAABAAAAAAAAAAAAABQABQEiAAAAAAAAAAAARAAAAAgD#####AAAAAAEQAAABAAAAAQAAAAoAP#AAAAAAAAD#####AAAAAQAJQ0NlcmNsZU9BAP####8AAAAAAAAAAQAAAAoAAAAcAAAAGwD#####AAAAHQAAAB4AAAAcAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAfAAAAHAD#####AAAAAAAQAAFKAEAQAAAAAAAAwDYAAAAAAAAAAAUAAgAAAB######AAAAAgAHQ1JlcGVyZQD#####AObm5gADcmVwAAEAAAAKAAAAHAAAACEBAQAAAAAMAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAMP#AAAAAAAAAAAAAMP#AAAAAAAAD#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAAABAAAACgAAABwAAAAgAP####8AAAAAABAAAAEAAAABAAAACgAAACEAAAAI##########8='
// const fig1 = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAUqAAAC4AAAAQEAAAAAAAAAAQAAACb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AANhMTEAATEAAAABP#AAAAAAAAAAAAACAP####8AA2ExMgABNQAAAAFAFAAAAAAAAAAAAAIA#####wADYTEzAAEzAAAAAUAIAAAAAAAAAAAAAgD#####AANhMjEAATEAAAABP#AAAAAAAAAAAAACAP####8AA2EyMgABMQAAAAE#8AAAAAAAAAAAAAIA#####wADYTIzAAEzAAAAAUAIAAAAAAAAAAAAAgD#####AANhMzEAATEAAAABP#AAAAAAAAAAAAACAP####8AA2EzMgABMQAAAAE#8AAAAAAAAAAAAAIA#####wADYTMzAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AAJiMQABMQAAAAE#8AAAAAAAAAAAAAIA#####wACYjIAATIAAAABQAAAAAAAAAAAAAACAP####8AAmIzAAEzAAAAAUAIAAAAAAAA#####wAAAAEACUNGb25jTlZhcgD#####AARyb3cxABRhMTEqeCthMTIqeSthMTMqej1iMf####8AAAABAApDT3BlcmF0aW9uCAAAAAQAAAAABAAAAAAEAv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAB#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAQCAAAABQAAAAIAAAAGAAAAAQAAAAQCAAAABQAAAAMAAAAGAAAAAgAAAAUAAAAKAAAAAwABeAABeQABegAAAAMA#####wAEcm93MgAUYTIxKngrYTIyKnkrYTIzKno9YjIAAAAECAAAAAQAAAAABAAAAAAEAgAAAAUAAAAEAAAABgAAAAAAAAAEAgAAAAUAAAAFAAAABgAAAAEAAAAEAgAAAAUAAAAGAAAABgAAAAIAAAAFAAAACwAAAAMAAXgAAXkAAXoAAAADAP####8ABHJvdzMAFGEzMSp4K2EzMip5K2EzMyp6PWIzAAAABAgAAAAEAAAAAAQAAAAABAIAAAAFAAAABwAAAAYAAAAAAAAABAIAAAAFAAAACAAAAAYAAAABAAAABAIAAAAFAAAACQAAAAYAAAACAAAABQAAAAwAAAADAAF4AAF5AAF6#####wAAAAQAD0NFZGl0ZXVyRm9ybXVsZQD#####AAAAAAEAAP####8OQEdAAAAAAABAOYUeuFHrhgAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAAABAAkkYV97MTF9PSQAAAAEAAEAAT0BAQAAAAcA#####wAAAAABAAD#####DkBn8AAAAAAAQDmFHrhR64YAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAAAAgAJJGFfezEyfT0kAAAABAABAAE9AQEAAAAHAP####8AAAAAAQAA#####w5AdVgAAAAAAEA3hR64UeuGAAAAAAACAAAAAAAAAAEAAAAAAAAAAAAAAAMACSRhX3sxM309JAAAAAQAAQABPQEBAAAABwD#####AAAAAAEAAP####8OQEdAAAAAAABAUWFHrhR64gAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAAAEAAkkYV97MjF9PSQAAAAEAAEAAT0BAQAAAAcA#####wAAAAABAAD#####DkBn8AAAAAAAQFFhR64UeuIAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAAABQAJJGFfezIyfT0kAAAABAABAAE9AQEAAAAHAP####8AAAAAAQAA#####w5AdVgAAAAAAEBQYUeuFHriAAAAAAACAAAAAAAAAAEAAAAAAAAAAAAAAAYACSRhX3syM309JAAAAAQAAQABPQEBAAAABwD#####AAAAAAEAAP####8OQEdAAAAAAABAXGFHrhR64gAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAAAHAAkkYV97MzF9PSQAAAAEAAEAAT0BAQAAAAcA#####wAAAAABAAD#####DkBn0AAAAAAAQFvhR64UeuIAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAAACAAJJGFfezMyfT0kAAAABAABAAE9AQEAAAAHAP####8AAAAAAQAA#####w5AdUgAAAAAAEBa4UeuFHriAAAAAAACAAAAAAAAAAEAAAAAAAAAAAAAAAkACSRhX3szM309JAAAAAQAAQABPQEBAAAABwD#####AAAAAAEAAP####8OQH6oAAAAAABAOIUeuFHrhgAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAAAKAAYkYl8xPSQAAAAEAAEAAT0BAQAAAAcA#####wAAAAABAAD#####DkB+mAAAAAAAQFAhR64UeuIAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAAACwAGJGJfMj0kAAAABAABAAE9AQEAAAAHAP####8AAAAAAQAA#####w5AfpgAAAAAAEBaIUeuFHriAAAAAAACAAAAAAAAAAEAAAAAAAAAAAAAAAwABiRiXzM9JAAAAAQAAQABPQEB#####wAAAAIABkNMYXRleAD#####AAAAAAEAAP####8QQC0AAAAAAABAY#Cj1wo9cQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAc1x0ZXh0e1N5c3TDqG1lIDogfQpcbGVmdFx7IFxiZWdpbnthcnJheX17bH0KXEZvclNpbXB7cm93MX0gClxcIFxGb3JTaW1we3JvdzJ9ClxcIFxGb3JTaW1we3JvdzN9ClxlbmR7YXJyYXl9IFxyaWdodC7#####AAAAAQAIQ01hdHJpY2UA#####wABQQAAAAMAAAADAAAABQAAAAEAAAAFAAAAAgAAAAUAAAADAAAABQAAAAQAAAAFAAAABQAAAAUAAAAGAAAABQAAAAcAAAAFAAAACAAAAAUAAAAJAAAACQD#####AAFCAAAAAwAAAAEAAAAFAAAACgAAAAUAAAALAAAABQAAAAz#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAMRMOpdGVybWluYW50AAAAAQAAAAEAAAABAAAAHf####8AAAABAAhDQ2FsY01hdAAAAAAfAAZkZXRNYXQACGRldGVyKEEp#####wAAAAIACUNGb25jdGlvbhcAAAAFAAAAHQAAAAIBAAAAHwAEZGV0QQALZGV0TWF0KDEsMSn#####AAAAAQAIQ1Rlcm1NYXQAAAAgAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AAZkZXRudWwABmRldEE9MAAAAAQIAAAABQAAACEAAAABAAAAAAAAAAAAAAALAP####8AAVgACEFeKC0xKSpCAAAABAL#####AAAAAQAKQ1B1aXNzYW5jZQAAAAUAAAAd#####wAAAAEADENNb2luc1VuYWlyZQAAAAE#8AAAAAAAAAAAAAUAAAAeAAAACAD#####AAAAAAEAAP####8QQCkAAAAAAABAcShR64UeuAAAAAAAAAAAAAAAAAABAAAAAAAAAAAA7FxJZntkZXRudWx9CnsKXHRleHR7TGUgc3lzdMOobWUgbidhIHBhcyB1biBzZXVsIHRyaXBsZXQgc29sdXRpb259Cn0KewpcYmVnaW57YXJyYXl9e2x9Clx0ZXh0e0xlIHN5c3TDqG1lIGEgdW4gc2V1bCB0cmlwbGV0IHNvbHV0aW9ufQpcXCBcbGVmdCggXGJlZ2lue2FycmF5fXtsfQp4IApcXCB5ClxcIHoKXGVuZHthcnJheX0gXHJpZ2h0KQo9ICBcbGVmdCggXFZhbEZyYWN7WH0gXHJpZ2h0KQpcZW5ke2FycmF5fQp9#####wAAAAIADENDb21tZW50YWlyZQD#####AP8AAAEAAP####8QQDOAAAAAAABAeKhR64UeuAAAAAAAAAAAAAAAAAABAAAAAAAAAAAA8E4nZW50cmV6IGNvbW1lIGNvZWZmaWNpZW50cyBxdWUgZGVzIG5vbWJyZXMgZW50aWVycy4KTGEgc29sdXRpb24gc2VyYSBlbmNvcmUgYm9ubmUgc2kgbGVzIGNvZWZmaWNpZW50cwpzb250IGRlcyBub21icmVzIHJhdGlvbm5lbHMgbWFpcyBsJ2FmZmljaGFnZQpkdSBzeXN0w6htZSBzZXJhIGFwcHJvY2jDqSAobWFpcyDDoCBjb25kaXRpb24gcXVlCmxlcyBjb2VmZmljaWVudHMgcmVzdGVudCBhc3NleiAic2ltcGxlcyIpLv###############w=='
// const fig1 = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wAAAAANAACIuAAAgCoAAH0XAACAJwAAgCgAAH0UAACACwAAgCUAAIANAACAEgAAgBAAAH0oAACAHQAAAAAC9gAAAjgAAAEBAAAAAAAAAAAAAAAi#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAABDgABVQDAJAAAAAAAAEAQAAAAAAAABQAAQCxmZmZmZmZALGZmZmZmZv####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAAQAAABAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFWAMAAAAAAAAAAQBAAAAAAAAAFAABAPGZmZmZmZgAAAAL#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAHQ01pbGlldQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAFDAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAATH#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAAAwAAAAIA#####wEAAAAAEAABQQAAAAAAAAAAAEAIAAAAAAAABQAAQGJgAAAAAABAYQAAAAAAAP####8AAAABAAdDQ2FsY3VsAP####8ABWxvbmcxAAE2AAAAAUAYAAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AG1NlZ21lbnQgZGUgbG9uZ3VldXIgZG9ubsOpZQAAAAYAAAADAAAAAgAAAAkAAAAI#####wAAAAIACUNDZXJjbGVPUgAAAAAKAQAAAAABAAAACP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAJAP####8AAAABAA9DUG9pbnRMaWVDZXJjbGUBAAAACgEAAAAAEAABQgBACAAAAAAAAAAAAAAAAAAABQABAAAAAAAAAAAAAAALAAAABQEAAAAKAQAAAAAQAAABAAEAAAAIAAAADAAAAAYAAAAACgEAAAAAEAAAAQUAAAAACAAAAAz#####AAAAAQALQ01lZGlhdHJpY2UAAAAACgEAAAAAEAAAAQABAAAACAAAAAwAAAALAAAAAAoBAAAAAAEAAAAOAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAAAoAAAAPAAAAEP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AAAAAAoBAAAAABAAAAEFAAEAAAAR#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAKAQAAAAEAAAASEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAAkAAAAKAP####8ADUNhcnLDqSBkaXJlY3QAAAAFAAAAAgAAAAIAAAAMAAAACAAAAAUAAAAAFAEAAAAAEAAAAQABAAAADAAAAAj#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQAAAAAUAAAAAAAQAAABAQEAAAAMAAAAFf####8AAAABAAlDQ2VyY2xlT0EAAAAAFAAAAAABAQAAAAwAAAAIAAAADwAAAAAUAAAAFgAAABcAAAAQAQAAABQBAAAAABAAAUMBBQACAAAAGP####8AAAABAAxDVHJhbnNsYXRpb24AAAAAFAAAAAwAAAAI#####wAAAAEAC0NQb2ludEltYWdlAQAAABQBAAAAABAAAUQBBQAAAAAZAAAAGv####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAQAAAAUAAAAMAAAACAAAABsAAAAZAAAADAAAAAYA#####wEAAAAAEAABSQAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAADAAAAAYA#####wEAAAAAEAABSgAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAAGwAAAAUA#####wEAAAAAEAAAAQABAAAAHQAAAB4AAAAFAP####8BAAAAABAAAAEAAQAAAAwAAAAb#####wAAAAEAEENNYWNyb0FwcGFyaXRpb24A#####wEAAAAB#####xBAewAAAAAAAEBjYAAAAAAAAgAAAAAAAAAAAAAAAAEAAAAAAAAAAAANI1NvbHV0aW9uSXNvIwAAAAAAAwAAABwAAAAgAAAAHwAAAAAH##########8='

// autre figure d'exemple, chaîne latex
export const fig2 = 'TWF0aEdyYXBoSmF2YTEuMAAAABUAAmZy+#vvAQD#AQAAAAAAAAAABQMAAALHAAABAQAAAAAAAAAAAAAAQP####8AAAABAApDQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEACkNDb25zdGFudGVACSH7VEQtGP####8AAAABAA9DVmFyaWFibGVCb3JuZWUA#####wABcEAIAAAAAAAAP#AAAAAAAABANAAAAAAAAD#wAAAAAAAAAQABMQACMjAAATEAAAACAP####8AAW5AAAAAAAAAAAAAAAAAAAAAQCQAAAAAAAA#8AAAAAAAAAEAATAAAjEwAAEx#####wAAAAEACkNQb2ludEJhc2UA#####wAAAAA#gAAAAA4AAU8AwCgAAAAAAAAAAAAAAAAAAAAABQABQICIAAAAAABAc1AAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAD+AAAAADgAAAQAAAAEAAAADAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAD+AAAABDgABSQDAGAAAAAAAAAAAAAAAAAAAAAAFAAFAPuZmZmZmZgAAAAT#####AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAP4AAAAAQAAABAAAAAQAAAAMAAAAF#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wAAAAA#gAAAAA4AAAEAAAABAAAAAwAAAAb#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAP4AAAAAAAAEAAAADAAAABf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAHAAAACP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAP4AAAAAOAAABAAAFAAEAAAAJAAAACgD#####AAAAAD+AAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAJ#####wAAAAIAB0NSZXBlcmUA#####wCkpKQ#gAAAAANyZXAAAQAAAAMAAAAFAAAACwAAAQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABAApDVW5pdGV4UmVwAP####8ABHVuaXQAAAAM#####wAAAAEAC0NIb21vdGhldGllAP####8AAAAD#####wAAAAEACkNPcGVyYXRpb24DAAAAAT#wAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAA3#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAA#gAAAABAAAlciAQAAAQAAAAAFAAAADv####8AAAABAAlDTG9uZ3VldXIA#####wAAAAMAAAAP#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAASAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AFEdyYWR1YXRpb25BeGVzUmVwZXJlAAAAGwAAAAgAAAADAAAADAAAABEAAAAS#####wAAAAEAE0NBYnNjaXNzZU9yaWdpbmVSZXAAAAAAEwAFYWJzb3IAAAAM#####wAAAAEAE0NPcmRvbm5lZU9yaWdpbmVSZXAAAAAAEwAFb3Jkb3IAAAAMAAAADAAAAAATAAZ1bml0ZXgAAAAM#####wAAAAEACkNVbml0ZXlSZXAAAAAAEwAGdW5pdGV5AAAADP####8AAAABABBDUG9pbnREYW5zUmVwZXJlAAAAABMAAAAAP4AAAAAOAAABAAAFAAAAAAwAAAAPAAAAFAAAAA8AAAAVAAAAFwAAAAATAAAAAD+AAAAADgAAAQAABQAAAAAMAAAADgAAAAAPAAAAFAAAAA8AAAAWAAAADwAAABUAAAAXAAAAABMAAAAAP4AAAAAOAAABAAAFAAAAAAwAAAAPAAAAFAAAAA4AAAAADwAAABUAAAAPAAAAFwAAAA0AAAAAEwAAABgAAAAPAAAAEQAAABAAAAAAEwAAAAA#gAAAAA4AAAEAAAUAAAAAGQAAABsAAAANAAAAABMAAAAYAAAADwAAABIAAAAQAAAAABMAAAAAP4AAAAAOAAABAAAFAAAAABoAAAAd#####wAAAAEACENTZWdtZW50AAAAABMBAAAAP4AAAAAQAAABAAAAAQAAABkAAAAcAAAAGAAAAAATAQAAAD+AAAAAEAAAAQAAAAEAAAAaAAAAHgAAAAUAAAAAEwEAAAA#gAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAAABQABP9xWeJq83w4AAAAf#####wAAAAIACENNZXN1cmVYAAAAABMABnhDb29yZAAAAAwAAAAhAAAAEgAAAAATAAVhYnN3MQAGeENvb3JkAAAADwAAACL#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAABMAZmZmP4AAAAAAAAAAIQAAAA8AAAARAAAAIQAAAAIAAAAhAAAAIQAAABIAAAAAEwAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAAOAQAAAA4CAAAAAUAAAAAAAAAAAAAADwAAABQAAAAPAAAAIwAAABcAAAAAEwEAAAA#gAAAAAsAAAEAAAUAAAAADAAAAA8AAAAlAAAADwAAABUAAAAaAQAAABMAZmZmP4AAAAAAAAAAJgAAAA8AAAARAAAAIQAAAAUAAAAhAAAAIgAAACMAAAAlAAAAJgAAAAUAAAAAEwEAAAA#gAAAAAsAAVIAQCAAAAAAAADAIAAAAAAAAAAABQABP9EbToG06B8AAAAg#####wAAAAIACENNZXN1cmVZAAAAABMABnlDb29yZAAAAAwAAAAoAAAAEgAAAAATAAVvcmRyMQAGeUNvb3JkAAAADwAAACkAAAAaAQAAABMAZmZmP4AAAAAAAAAAKAAAAA8AAAASAAAAKAAAAAIAAAAoAAAAKAAAABIAAAAAEwAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAAOAQAAAA4CAAAAAUAAAAAAAAAAAAAADwAAABUAAAAPAAAAKgAAABcAAAAAEwEAAAA#gAAAAAsAAAEAAAUAAAAADAAAAA8AAAAUAAAADwAAACwAAAAaAQAAABMAZmZmP4AAAAAAAAAALQAAAA8AAAASAAAAKAAAAAUAAAAoAAAAKQAAACoAAAAsAAAALf####8AAAACAAxDQ29tbWVudGFpcmUAAAAAEwFmZmY#gAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAACELAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAAACyNWYWwoYWJzdzEpAAAAGgEAAAATAGZmZj+AAAAAAAAAAC8AAAAPAAAAEQAAACEAAAAEAAAAIQAAACIAAAAjAAAALwAAABwAAAAAEwFmZmY#gAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAACYLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAAACyNWYWwoYWJzdzIpAAAAGgEAAAATAGZmZj+AAAAAAAAAADEAAAAPAAAAEQAAACEAAAAGAAAAIQAAACIAAAAjAAAAJQAAACYAAAAxAAAAHAAAAAATAWZmZj+AAAAAwCAAAAAAAAA#8AAAAAAAAAAAAAAAKAsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAALI1ZhbChvcmRyMSkAAAAaAQAAABMAZmZmP4AAAAAAAAAAMwAAAA8AAAASAAAAKAAAAAQAAAAoAAAAKQAAACoAAAAzAAAAHAAAAAATAWZmZj+AAAAAwBwAAAAAAAAAAAAAAAAAAAAAAAAALQsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAALI1ZhbChvcmRyMikAAAAaAQAAABMAZmZmP4AAAAAAAAAANQAAAA8AAAASAAAAKAAAAAYAAAAoAAAAKQAAACoAAAAsAAAALQAAADUAAAASAP####8AAWEAATEAAAABP#AAAAAAAAD#####AAAABAAPQ0VkaXRldXJGb3JtdWxlAP####8AAAAAP4AAAAEAAP####8OQEMAAAAAAABAOwAAAAAAAAAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAAAANwAEYSA9IAAAAAwAAQABPQEBAAAAAwD#####AAAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBVwAAAAAAAQCoAAAAAAAAAAAADAP####8AAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQGEgAAAAAABAXAAAAAAAAAAAAAYA#####wAAAAA#gAAAABAAAAEAAAABAAAAOQAAADoAAAADAP####8AAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHPgAAAAAABAUcAAAAAAAAAAAAMA#####wAAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAi1AAAAAAAECA0AAAAAAAAAAACAD#####AAAAAD+AAAAAAAABAAAAOgAAADz#####AAAAAgAGQ0xhdGV4AP####8AAAAAP4AAAAEAAP####8QQH3IAAAAAABAQQUeuFHrhQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAthID0gXFZhbHthfQAAABD##########w=='

export const figArbrePythag0 = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAANmcmH###8BAP8BAAAAAAAAAAAFHgAAAsoAAAAAAAAAAQAAAAYABlB5dGhhZwAAAAAAAgAAAAcAAAAADv####8AAAABABFDRWxlbWVudEdlbmVyaXF1ZQAAAAAAAP####8AAAAAAAAAAAAA##########8AAAABAAxDVHJhbnNsYXRpb24A#####wAAAAAAAAAB#####wAAAAEACUNSb3RhdGlvbgD#####AAAAAP####8AAAABAApDQ29uc3RhbnRlQFaAAAAAAAD#####AAAAAQALQ1BvaW50SW1hZ2UB#####wAAAAAADAAAAQAABQAAAAABAAAAAwAAAAQA#####wAAAAAADAAAAQAABQAAAAAEAAAAAv####8AAAABAAtDU2ltaWxpdHVkZQD#####AAAABAAAAANARoAAAAAAAP####8AAAABAApDT3BlcmF0aW9uAwAAAAM#8AAAAAAAAP####8AAAACAAlDRm9uY3Rpb24BAAAAA0AAAAAAAAAAAAAABAH#####AAAAAAAMAAABAAAFAAAAAAUAAAAGAAAAAQD#####AAAABAAAAAUAAAAEAf####8AAAAAAAwAAAEAAAUAAAAABAAAAAj#####AAAAAQAJQ1BvbHlnb25lAf####8AAAD#AAAAAQAAAAQAAAAEAAAACQAAAAcAAAAE#####wAAAAEAEENTdXJmYWNlUG9seWdvbmUB#####wAAAP8AAAAAAAUAAAAKAAAACAH#####AH8AAAAAAAEAAAAFAAAAAAAAAAEAAAAJAAAABAAAAAAAAAAJAf####8AfwAAAAAAAAAFAAAADAAAAAEAAAAP#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDYAAAADQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAPAAFBAQAABQABQECAAAAAAABAaAAAAAAAAAAAAAsA#####wAAAAAADwABQgEAAAUAAUBhQAAAAAAAQGgAAAAAAAAAAAABAP####8AAAABAAAAAgAAAAIA#####wAAAAEAAAADQFaAAAAAAAAAAAAEAP####8AAAAAAA8AAUQAwDcAAAAAAADALgAAAAAAAAAABQAAAAACAAAABAAAAAQA#####wAAAAAADwABQwBAHAAAAAAAAMAqAAAAAAAAAAAFAAAAAAUAAAADAAAABQD#####AAAABQAAAANARoAAAAAAAAAAAAYDAAAAAz#wAAAAAAAAAAAABwEAAAADQAAAAAAAAAAAAAAEAP####8AAAAAAA8AAUUAwCAAAAAAAADANgAAAAAAAAAABQAAAAAGAAAABwAAAAEA#####wAAAAUAAAAGAAAABAD#####AAAAAAAMAAABAAAFAAAAAAUAAAAJAAAACAD#####AAAA#wAAAAEAAAAEAAAABQAAAAoAAAAIAAAABQAAAAkA#####wAAAP8AAAAAAAUAAAALAAAACAD#####AH8AAAAAAAEAAAAFAAAAAQAAAAIAAAAKAAAABQAAAAEAAAAJAP####8AfwAAAAAAAAAFAAAADf###############w=='

export const figRepereVide = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAUeAAACygAAAQEAAAAAAAAAAAAAADX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAAAAUAAUCAiAAAAAAAQHNQAAAAAAD#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAADgAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFJAMAYAAAAAAAAAAAAAAAAAAAAAAUAAUA+5mZmZmZmAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAAA4AAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAADgAAAQAABQABAAAABwAAAAkA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAH#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAA3JlcAABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAoAAAAPAAAAEP####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABEABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABEABW9yZG9yAAAACgAAAAsAAAAAEQAGdW5pdGV4AAAACv####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAAr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAOAAABAAAFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAOAAABAAAFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAADgAAAQAABQAAAAAKAAAADgAAABIAAAANAAAAAA4AAAATAAAADgAAABUAAAAMAAAAABEAAAAWAAAADgAAAA8AAAAPAAAAABEAAAAAAA4AAAEAAAUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAAA4AAAEAAAUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQAAAAEAAAAXAAAAGgAAABcAAAAAEQEAAAAAEAAAAQAAAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQBmZmYAAAAAAB8AAAAOAAAADwAAAB8AAAACAAAAHwAAAB8AAAARAAAAABEABWFic3cyAA0yKmFic29yLWFic3cxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAASAAAADgAAACEAAAAWAAAAABEBAAAAAAsAAAEAAAUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEAZmZmAAAAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAAAAUAAT#RG06BtOgfAAAAHv####8AAAACAAhDTWVzdXJlWQAAAAARAAZ5Q29vcmQAAAAKAAAAJgAAABEAAAAAEQAFb3JkcjEABnlDb29yZAAAAA4AAAAnAAAAGQEAAAARAGZmZgAAAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAACwAAAQAABQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQBmZmYAAAAAACsAAAAOAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEAZmZmAAAAAAAtAAAADgAAAA8AAAAfAAAABAAAAB8AAAAgAAAAIQAAAC0AAAAbAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAACQLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAZAQAAABEAZmZmAAAAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAAAAAJgsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIxKQAAABkBAAAAEQBmZmYAAAAAADEAAAAOAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABsAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABkBAAAAEQBmZmYAAAAAADMAAAAOAAAAEAAAACYAAAAGAAAAJgAAACcAAAAoAAAAKgAAACsAAAAzAAAADv##########'

export const jugglingPython = `
from browser import timer

"""
Animation inspirée de https://www.geogebra.org/python/index.html
"""

import math

ph = addCalc('ph', '0')
valph = 0

def draw():
    global valph
    valph += 0.0075
    giveFormulaTo('ph', str(valph))
    updateDependantDisplay(ph) # On ne calcule et redessine que les objets dépenant du calcul ph
    
dimf = getFigDim() # Renvoie une liste contenant les dimensions de la fenêtre de travail
width = dimf[0]
height = dimf[1]
zoomFig(width/2, height/2, 2) # On zoome d'un facteur 2 sur la figure
addTimerButton(draw, 1/60)
O = getPointByName('O') # On mémorise le point O origine du repère comme objet point (pllus rapide que de faire référence à son nom plus tard)
setAngleUnity('radian') # On impose le radian à notre figure
head_0 = addPointXY(0, 2)
setHidden(head_0)
head_radius = 0.8
head = addCircleOr(head_0, head_radius, 'black', '-', 2)
head_0_position = getPointPosition(head_0)
head_base = addPointXY(head_0_position.x, head_0_position.y - head_radius)
setHidden(head_base)

spine_base = addPointXY(0, head_0_position.y - 4)
setHidden(spine_base)
torso = addSegment(head_base, spine_base, 'black', '-', 2)

# "Left" and "right" are from the juggler's point of view:
shoulder_y = head_0_position.y - 1.5
l_shoulder = addPointXY(1.25, shoulder_y, '', 'black', 'O')
r_shoulder = addPointXY(-1.25, shoulder_y, '', "black", 'O')

shoulders = addSegment(l_shoulder, r_shoulder, 'black', '-', 2)
up_arm_0 = addPointXY(0.0, -1.0)
setHidden(up_arm_0)
    
n_balls = addCalc('nballs', '5') # Modifiable mais doit rester impair et >= 3!
nballs = getValue(n_balls)
l_arm_ph = addCalc('larmph', 'mod(nballs * ph,1)')
l_arm_th = addCalc('larmth', '2.0 * pi * larmph')

l_up_th = addCalc('lupth', '-0.25 * larmph * sin(larmth)')
v_l_up_arm = addImPointRotation(up_arm_0, O, l_up_th)
setHidden(v_l_up_arm)
l_elbow = addImPointTranslation(v_l_up_arm, O, l_shoulder)

l_forearm_x = addCalc('lforearmx', '-0.75 * cos(larmth)')
l_forearm_y = addCalc('lforearmy', '0.5 * (sin(larmth) - 0.8)')

v_l_forearm = addPointXY(l_forearm_x, l_forearm_y)
setHidden(v_l_forearm)
l_hand = addImPointTranslation(v_l_forearm, O, l_elbow)
mesabsleft = addXMeasure(l_hand, 'masabsleft')
mesordleft = addYMeasure(l_hand, 'masordleft')
l_hand_comp = addCalcComp('lhand', 'masabsleft + i*masordleft')

l_up_arm = addSegment(l_shoulder, l_elbow, 'green', '-', 2)
l_forearm = addSegment(l_elbow, l_hand, 'green', '-', 2)

r_arm_ph = addCalc('rarmph', 'mod(larmph + 0.5, 1)')
r_arm_th = addCalc('rarmth', '2.0 * pi * rarmph')

r_up_th = addCalc('rupth', '0.25 * rarmph * sin(rarmth)')

v_r_up_arm = addImPointRotation(up_arm_0, O, r_up_th)
setHidden(v_r_up_arm)
r_elbow = addImPointTranslation(v_r_up_arm, O, r_shoulder)

r_forearm_x = addCalc('rforearmx', '0.75 * cos(rarmth)')
r_forearm_y = addCalc('rforearmy', '0.5 * (sin(rarmth) - 0.8)')
v_r_forearm = addPointXY(r_forearm_x, r_forearm_y)
setHidden(v_r_forearm)
r_hand = addImPointTranslation(v_r_forearm, O, r_elbow)
mesabsright = addXMeasure(r_hand, 'mesabsright')
mesordright = addYMeasure(r_hand, 'mesordright')
r_hand_comp = addCalcComp('rhand', 'mesabsright+i*mesordright')
r_up_arm = addSegment(r_shoulder, r_elbow, 'red', '-', 2)
r_forearm = addSegment(r_elbow, r_hand, 'red', '-', 2)

ball_colors = ["blue", "green", "red", "#ffbf00", "cyan"]

# Dans cette fonction, il faut donner aux objets de type calcul MathGrapH32 des noms dépendant de i car sinon on aura dex erreurs de calcul créés ayant le même nom
def mk_ball(i):
    stri = str(i)
    j = 'j' + stri # Nom du calcul qui représnete la valeur actuelle de i dans les calculs MathGraph32 suivants
    addCalc(j, stri)
    ballph = 'ballph' + stri # Nom de calcul dans mtg32
    ball_ph = addCalc(ballph, f"mod(2.0 * (ph * nballs + {j}),nballs * 2)") 
    airph = 'airph' + stri # Nom de calcul dans mtg32
    # f suivi de " sert à insérer dans la chaîne entre "" le contenu de variables chaînes en entournat leur nom entre accolades #  
    air_ph = addCalc(airph, f"mod({ballph}, nballs) / (nballs - 1)")
    airltorx = 'airltorx' + stri  # Nom de calcul dans mtg32
    air_l_to_r_x = addCalc(airltorx, f"0.5 - {airph} * 2.5")
    airrtolx = 'airrtolx' + stri # Nom de calcul dans mtg32
    air_r_to_l_x = addCalc(airrtolx, f"-0.5 + {airph} * 2.5")
    airy = 'airy' + stri # Nom de calcul dans mtg32
    air_y = addCalc(airy, f"2 + 1.0 - 15.0 * ({airph} - 0.5)^2")
    airltor = 'airltor' + stri # Nom de calcul complexe dans mtg32
    air_l_to_r = addCalcComp(airltor, f"{airltorx}+i*{airy}")
    airrtol = 'airrtol' + stri # Nom de calcul complexe dans mtg32
    air_r_to_l = addCalcComp(airrtol, f"{airrtolx}+i*{airy}")
    # Ci-dessous on définit une formule de calcul complexe valide pour MathGraph32
    forcalc = f"si({ballph} <= nballs - 1, {airltor}, si({ballph} <= nballs ,rhand, si({ballph} <= 2*nballs - 1,{airrtol}, lhand)))"

    color = ball_colors[i % 5]
    point = addPointZ(f"{forcalc}", '') # Le point est défini par son affixe complexe donné par la formule forcalc
    setHidden(point)
    circle = addCircleOr(point, 0.15, 'black', '-', 2)
    return addSurface(circle, color, 'fill')


for i in range(nballs):
    mk_ball(i)
`

export const arbrePythagorePython = `
colors = ['red', 'blue', 'cyan', 'red', 'magenta', 'maroon']
def color(index):
    i = (len(colors) - index) % len(colors)
    return colors[i]
setAngleUnity('degree')
[width, height] = getFigDim()
A = addFreePoint({'x':6*width/14, 'y':height - 20, 'name':'A', 'color':'red', 'absCoord': True})
B = addFreePoint({'x':8*width/14, 'y':height - 20, 'name':'B', 'color':'red', 'absCoord': True})
ang45 = addCalc('ang45', '45')
ang90 = addCalc('ang90', '90')
k = addCalc('k', 'sqrt(2)/2')
def tree(A, B, prof):
    D = addImPointRotation(B, A, ang90); setHidden(D)
    C = addImPointTranslation (D, A, B); setHidden(C)
    col1 = color(prof)
    poly = addPolygon([A, B, C, D], col1)
    addSurface(poly, col1)
    col2 = color(prof - 1)
    sim = addSimilitude(D, ang45, k)
    E = addPointIm(C, sim); setHidden(E)
    poly2 = addPolygon([C, D, E], col2)
    addSurface(poly2, col2)
    if (prof != 0):
        tree(E, C, prof - 1)
        tree(D, E, prof - 1)


def displayButtonsOnTop():
   displayOnTop('btn0')
   displayOnTop('btn1')
   displayOnTop('btn2')
   displayOnTop('btn3')
   displayOnTop('btn4')
   displayOnTop('btn5')
   displayOnTop('btn9')
   displayOnTop('btn10')
   displayOnTop('btn11')
def tree0():
   deleteAfter('W')
   tree(A, B, 0)
   displayButtonsOnTop()
def tree1():
   deleteAfter('W')
   tree(A, B, 1)
   displayButtonsOnTop()
def tree2():
   deleteAfter('W')
   tree(A, B, 2)
   displayButtonsOnTop()
def tree3():
   deleteAfter('W')
   tree(A, B, 3)
   displayButtonsOnTop()
def tree4():
   deleteAfter('W')
   tree(A, B, 4)
   displayButtonsOnTop()
def tree5():
   deleteAfter('W')
   tree(A, B, 5)
   displayButtonsOnTop()
def tree9():
   deleteAfter('W')
   tree(A, B, 9)
   displayButtonsOnTop()
def tree10():
   deleteAfter('W')
   tree(A, B, 10)
   displayButtonsOnTop()
def tree11():
   deleteAfter('W')
   tree(A, B, 11)
   displayButtonsOnTop()
btn0 = addActionButton('tree prof = 0', tree0, 10, 10, 'btn0')
btn1 = addActionButton('tree prof = 1', tree1, 10, 40, 'btn1')
btn2 = addActionButton('tree prof = 2', tree2, 10, 70, 'btn2')
btn3 = addActionButton('tree prof = 3', tree3, 10, 100, 'btn3')
btn4 = addActionButton('tree prof = 4', tree4, 10, 130, 'btn4')
btn5 = addActionButton('tree prof = 5', tree5, 10, 160, 'btn5')
btn9 = addActionButton('tree prof = 9', tree9, 10, 190, 'btn9')
btn10 = addActionButton('tree prof = 10', tree10, 10, 220, 'btn10')
btn11 = addActionButton('tree prof = 11', tree11, 10, 250, 'btn11')
W = addFreePoint(0, 0, 'W')
setHidden(W)
tree10()
`
