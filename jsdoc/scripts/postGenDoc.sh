#!/bin/bash

[ ! -f "jsdoc/scripts/$(basename $0)" ] && echo "Il faut lancer ce script depuis la racine" >&2 && exit 1

# On génère deux documentations, celle des classes de mathgraph dans documentation/full, et celle du chargement dans documentation/loading
# il manque un /documentation/index.html que l'on ajoute ici
cp -f jsdoc/scripts/index.html documentation/
